#include "imagelibrarymodel.h"
#include <QDebug>

ImageLibraryModel::ImageLibraryModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int ImageLibraryModel::rowCount(const QModelIndex& ) const
{
    return images.count();
}

QVariant ImageLibraryModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    int row = index.row();

    if(role == Qt::DisplayRole)
    {
        return images[row]->objectName();
    }

    if(role == Qt::DecorationRole)
    {
        QPixmap pixmap(100, 100);
        QPainter painter(&pixmap);
        images[row]->render(&painter);

        QIcon icon(pixmap);

        return icon;
    }

    if(role == Qt::EditRole)
    {
        return images[row]->objectName();
    }

    if(role == Qt::ToolTipRole)
    {
        return images[row]->objectName();
    }

    return QVariant();
}

QVariant ImageLibraryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(orientation == Qt::Horizontal)
    {
        switch(section)
        {
        case 0:
            return ("Image");

        default:
            return QVariant();
        }
    }
    else
    {
        return QString("Color %1").arg(section);
    }
}

void ImageLibraryModel::AddItem(QString file) {
    QSvgRenderer *r = new QSvgRenderer();
    if (!r->load(file)) return;

    r->setObjectName(file.split(QChar('/')).last());
    int count = images.count();
    beginInsertRows(QModelIndex(), count, count + 1);
    images.append(r);
    endInsertRows();

}

QSvgRenderer* ImageLibraryModel::GetItem(int index) {
    if (index >= images.count()) return 0;
    return images[index];
}
