#-------------------------------------------------
#
# Project created by QtCreator 2013-08-30T18:47:38
#
#-------------------------------------------------

QT       += core gui svg

TARGET = galliard

DESTDIR = build/
OBJECTS_DIR = build/obj/
MOC_DIR = build/moc/
RCC_DIR = build/rcc/
UI_DIR = build/ui/

TEMPLATE = app
INCLUDEPATH += include\

FORMS += \
    forms/mainwindow.ui \
    forms/imagelibrary.ui

HEADERS += \
    include/mainwindow.h \
    include/imagelibrarymodel.h \
    include/imagelibrary.h \
    include/editablescene.h \
    include/creator.h \
    include/itemprimitiv.h

SOURCES += \
    src/mainwindow.cpp \
    src/main.cpp \
    src/imagelibrarymodel.cpp \
    src/imagelibrary.cpp \
    src/editablescene.cpp \
    src/creator.cpp \
    src/itemprimitiv.cpp




