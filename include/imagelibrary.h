#ifndef IMAGELIBRARY_H
#define IMAGELIBRARY_H

#include <QDialog>
#include <QEventLoop>
#include "imagelibrarymodel.h"

namespace Ui {
class ImageLibrary;
}

class ImageLibrary : public QDialog
{
    Q_OBJECT
    
public:
    explicit ImageLibrary(QWidget *parent = 0);
    ~ImageLibrary();
    QSvgRenderer *Exec();
    
private slots:


    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_BtnAddImg_clicked();


    void on_ViewListAddedImages_clicked(const QModelIndex &index);

private:
    Ui::ImageLibrary *ui;
    QEventLoop EventLoop;
    ImageLibraryModel ImageLibModel;
    bool ItemIsSelected;
    int SelectedIndex;
};

#endif // IMAGELIBRARY_H
