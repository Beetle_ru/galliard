#include "creator.h"
#include <QHash>
#include <QDebug>

Creator::Creator(QObject *parent) :
    QObject(parent)
{
}

QGraphicsItem* Creator::CreateCurrent() {
    if (CurrentItemType == ItemPrimitiv::NoItem) return 0;

    ItemPrimitiv *item = new ItemPrimitiv(CurrentItemType, CurrentItemProperty, Scene);
    QHash<QString, QVariant> *prop = item->GetProperty();

    QList<QString> keys = CurrentItemProperty.keys();
    for (int i = 0; i < keys.count(); i++) {
        QString key(keys[i]);
        (*prop)[key] = CurrentItemProperty[key];
    }
    item->UpdateItem();
    Items.append(item);
    return item->GetMainGraphicsItem();
}

void Creator::SetScene(QGraphicsScene *scene) {
    Scene = scene;
}

void Creator::SetCurrentItem(ItemPrimitiv::ItemType type, QHash<QString, QVariant> property) {
    CurrentItemType = type;
    CurrentItemProperty = property;
}

void Creator::SetCurrentItem(ItemPrimitiv::ItemType type) {
    CurrentItemType = type;
    CurrentItemProperty.clear();
}
