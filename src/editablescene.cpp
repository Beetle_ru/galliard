#include "editablescene.h"
 #include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QtGui>
//#include <QtScript>
#include <QSize>
#include <QMovie>

#include "imagelibrary.h"

EditableScene::EditableScene(QObject *parent) :
    QGraphicsScene(parent)
{
    IsScale = false;
    IsRotate = false;
}

int EditableScene::Rect(int x, int y, int xx, int yy, int parent) {
    SelectedItem = new QGraphicsRectItem(x,y,xx,yy,(QGraphicsRectItem*)parent,this);
    return (int)SelectedItem;
}

void EditableScene::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_S) {
        IsScale = true;
    }
    if (event->key() == Qt::Key_R) {
        IsRotate = true;
    }
}

void EditableScene::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_S) {
        IsScale = false;
    }
    if (event->key() == Qt::Key_R) {
        IsRotate = false;
    }
}

void EditableScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {



    //this->addRect(50,50,100,100);

    qreal x = mouseEvent->scenePos().x();
    qreal y = mouseEvent->scenePos().y();

    MousePositionX = x;
    MousePositionY = y;

    if (mouseEvent->button() == Qt::RightButton) qDebug("RightButton");
    if (mouseEvent->button() == Qt::LeftButton) qDebug("LeftButton");

    QString str;
    str.append(("mousePressEvent; "));
    str.append(QString("x:%1; ").arg(x));
    str.append(QString("y:%1; ").arg(y));
    qDebug(str.toAscii());
    //qDebug("mousePressEvent");
    ItemUnderMouse = false;
    for (int i = 0; i < this->items().count(); i++) {
        SelectedItem = this->items().at(i);
        if(SelectedItem->isUnderMouse()) {
            qDebug("isUnderMouse");
            ItemUnderMouse = true;
            break;
        }
    }
    if (!ItemUnderMouse) {

        //SelectedItem = CreateGraphicsObject();
        SelectedItem = ItemCreator->CreateCurrent();
        if (!SelectedItem) return;
        this->addItem(SelectedItem);
        qreal oldX = SelectedItem->pos().x();
        qreal oldY = SelectedItem->pos().y();
        //SelectedItem->moveBy(x - oldX, y - oldY);
        qreal offsetX = SelectedItem->boundingRect().width() * 0.5;
        qreal offsetY = SelectedItem->boundingRect().height() * 0.5;
        SelectedItem->moveBy(x - offsetX, y - offsetY);
        //SelectedItem->translate(x,y);
    }

    this->update();
}

void EditableScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    qDebug("mouseReleaseEvent");
    ItemUnderMouse = false;
    this->update();
}

void EditableScene::wheelEvent(QGraphicsSceneWheelEvent *mouseWhelEvent) {

    qreal delta = mouseWhelEvent->delta();
    QString str;
    str.append(("wheelEvent; "));
    str.append(QString("delta:%1; ").arg(delta));
    qDebug(str.toAscii());
    //if (ItemUnderMouse) {
    if(delta > 0)
        SelectedItem->scale(1.1,1.1);
    else
        SelectedItem->scale(0.9,0.9);
    //}
    this->update();
}

void EditableScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    //mouseEvent->scenePos().
    qreal newX = mouseEvent->scenePos().x();
    qreal newY = mouseEvent->scenePos().y();

    QString str;
    if (ItemUnderMouse) {
        qreal oldX = SelectedItem->pos().x();
        qreal oldY = SelectedItem->pos().y();
        qreal offsetX = SelectedItem->boundingRect().width() * 0.5;
        qreal offsetY = SelectedItem->boundingRect().height() * 0.5;

        if (IsScale) {
            //SelectedItem->scale((newX / MousePositionX), (newY / MousePositionY));
            SelectedItem->translate(offsetX, offsetY);
            qreal scale = (newX / MousePositionX) * (newY / MousePositionY);
            SelectedItem->scale(scale, scale);
            SelectedItem->translate(-offsetX,-offsetY);
        }
        else if (IsRotate) {
            //SelectedItem->setTransformOriginPoint(SelectedItem->boundingRect().center());

            SelectedItem->translate(offsetX, offsetY);
            SelectedItem->rotate(newX - MousePositionX + newY - MousePositionY);
            SelectedItem->translate(-offsetX,-offsetY);

            //SelectedItem->setRotation(SelectedItem->rotation() + newX - MousePositionX + newY - MousePositionY);

        }
        else {
        //SelectedItem->moveBy(newX - oldX - 25, newY - oldY - 25);
        SelectedItem->moveBy(newX - MousePositionX, newY - MousePositionY);
        }

        MousePositionX = newX;
        MousePositionY = newY;
        //SelectedItem->translate(newX,newY);


        str.append(("mouseMoveEvent; "));
        str.append(QString("newX:%1; ").arg(newX));
        str.append(QString("newY:%1; ").arg(newY));
        str.append(QString("oldX:%1; ").arg(oldX));
        str.append(QString("oldY:%1; ").arg(oldY));
    }
    qDebug(str.toAscii());
    this->update();
}

bool EditableScene::eventFilter(QObject *watched, QEvent *event) {
    //qDebug(watched->objectName().toAscii());

    qDebug() << event->type();
    if (watched->objectName() == "graphicsView") {
        if (event->type() == QEvent::KeyPress) {
            //qDebug("#########################");
            return true;
        }


    }
    //if (event->type() == QEvent::Wheel) qDebug("***");*/
    return false;
}

QGraphicsItem* EditableScene::CreateGraphicsObject() {
    QGraphicsSvgItem *svgItem;

    QLabel *gif_anim = new QLabel();
    QMovie *movie = new QMovie("t.gif");
    gif_anim->setMovie(movie);
    //gif_anim->setFocusPolicy( Qt::NoFocus );
    movie->start();
    QGraphicsProxyWidget *proxy;

    switch (ActiveCreatetingType) {
    case EditableScene::Empty:
        return 0;
        break;
    case EditableScene::Rectangle:
        //return new QGraphicsRectItem(0, 0, 50, 50);
        //return this->addPixmap(QPixmap("t.gif"));
        proxy = this->addWidget(gif_anim);
        //proxy->set
        return proxy;
        break;
    case EditableScene::Circle:
        return new QGraphicsEllipseItem(0, 0, 50, 50);
        //return
        break;
    case EditableScene::Image:
        if (CurrentSvgRenderr == 0) return 0;
         svgItem = new QGraphicsSvgItem();
         svgItem->setSharedRenderer(CurrentSvgRenderr);
        return svgItem;
        break;
    case EditableScene::Indicator:
        break;
    default:
        return 0;
        break;
    }

    return 0;
}

void EditableScene::SetCreatingType(CreateType type) {
    ActiveCreatetingType = type;
}

void EditableScene::SetCurrentSvgRenderr(QSvgRenderer *r) {
    CurrentSvgRenderr = r;
}

void EditableScene::SetCreator(Creator *itemCreator) {
    ItemCreator = itemCreator;
}



