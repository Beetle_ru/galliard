#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "editablescene.h"
#include <QDebug>
#include <QtGui>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);




    //QDeclarativeEngine *engine = new QDeclarativeEngine;
    //QDeclarativeComponent component(engine, QUrl::fromLocalFile(...));
    //QGraphicsObject *object =
    //qobject cast<QGraphicsObject*>(component.create());

    //QGraphicsScene *scene = new QGraphicsScene();
     //scene->addText("Hello, world!");
    //ui->graphicsView->setScene(scene);
    Scenes.append( new EditableScene()); // make zero scene
    SceneCurrentIndex = 0;

    //scene->setSceneRect(ui->graphicsView->rect());
    CurrentScene()->setSceneRect(ui->graphicsView->frameGeometry());
    ui->graphicsView->setScene(CurrentScene());

    ItemCreator.SetScene(CurrentScene());
    CurrentScene()->SetCreator(&ItemCreator);
    ImageSelector.setModal(true);
    //ImageSelector.setParent(this);
    //ui->graphicsView->setFocusPolicy( Qt::NoFocus );


    //scene->addItem();

    //ui->graphicsView->installEventFilter(scene);
    //this->installEventFilter(scene);


}

MainWindow::~MainWindow()
{
    delete ui;
}

EditableScene* MainWindow::CurrentScene() {
    if (Scenes.count() > SceneCurrentIndex)
        return Scenes[SceneCurrentIndex];
    qDebug("Bad index 'SceneCurrentIndex'");
    return 0;
}


void MainWindow::on_BtnSetRectangle_clicked()
{
    //CurrentScene()->SetCreatingType(EditableScene::Rectangle);
    ItemCreator.SetCurrentItem(ItemPrimitiv::Rectangle);

    //*******GIF TEST*************//
    //QHash<QString, QVariant> prop;
    //prop["Src"] = "t.gif";
    //ItemCreator.SetCurrentItem(ItemPrimitiv::GifImage, prop);
}

void MainWindow::on_BtnSetCircle_clicked()
{
    CurrentScene()->SetCreatingType(EditableScene::Circle);
    ItemCreator.SetCurrentItem(ItemPrimitiv::Circle);
}

void MainWindow::on_BtnSetImage_clicked()
{
    //ImageLibrary *ImageSelector = new ImageLibrary(this);
    //ImageSelector->setModal(true);
    QSvgRenderer *r = ImageSelector.Exec();

    //delete (ImageSelector);
    if (r == 0) return;
    CurrentScene()->SetCreatingType(EditableScene::Image);
    CurrentScene()->SetCurrentSvgRenderr(r);
}

void MainWindow::on_BtnSetIndicator_clicked()
{
    CurrentScene()->SetCreatingType(EditableScene::Indicator);
}
