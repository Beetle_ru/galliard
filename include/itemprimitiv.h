#ifndef ITEMPRIMITIV_H
#define ITEMPRIMITIV_H

#include <QObject>
#include <QHash>
#include <QVariant>
#include <QGraphicsItem>
#include <QBrush>
#include <QGraphicsScene>
#include <QtGui>
#include <QMovie>

class ItemPrimitiv : public QObject
{
    Q_OBJECT
public:
    enum ItemType{NoItem, Rectangle, Circle, GifImage};

    explicit ItemPrimitiv(QObject *parent = 0);
    ItemPrimitiv(ItemType type, QHash<QString, QVariant> property, QGraphicsScene *scene, QObject *parent = 0);
    QGraphicsItem* GetMainGraphicsItem();
    QHash<QString, QVariant>* GetProperty();
    void UpdateItem();
    void UpdateProp();

signals:
    
public slots:

private:
    QHash<QString, QVariant> Property; //item property(color fill, color line, position, size, etc)
    QList<QGraphicsItem*> GItem;
    ItemType Type;

};
#endif // ITEMPRIMITIV_H
