#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qlist.h>
#include "editablescene.h"
#include "imagelibrary.h"
#include "creator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    EditableScene* CurrentScene();
    Creator ItemCreator;
    
private slots:

    void on_BtnSetRectangle_clicked();


    void on_BtnSetCircle_clicked();

    void on_BtnSetImage_clicked();

    void on_BtnSetIndicator_clicked();

private:
    Ui::MainWindow *ui;
    QList<EditableScene*> Scenes;
    int SceneCurrentIndex;
    ImageLibrary ImageSelector;
    //QDeclarativeEngine EngineQml;
};

#endif // MAINWINDOW_H
