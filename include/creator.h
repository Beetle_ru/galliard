#ifndef CREATOR_H
#define CREATOR_H

#include <QObject>
#include <QVariant>
#include <QList>
#include <QGraphicsItem>
#include "itemprimitiv.h"
#include <QGraphicsScene>

class Creator : public QObject
{
    Q_OBJECT
public:
    explicit Creator(QObject *parent = 0);
    QGraphicsItem *CreateCurrent();
    void SetScene(QGraphicsScene *scene);

    
signals:
    
public slots:
    void SetCurrentItem(ItemPrimitiv::ItemType type, QHash<QString, QVariant> property);
    void SetCurrentItem(ItemPrimitiv::ItemType type);

private:
    QList<ItemPrimitiv*> Items;
    //ItemPrimitiv *CurrentItem;
    ItemPrimitiv::ItemType CurrentItemType;
    QHash<QString, QVariant> CurrentItemProperty;
    QGraphicsScene *Scene;
    
};

#endif // CREATOR_H
