#include "itemprimitiv.h"

ItemPrimitiv::ItemPrimitiv(QObject *parent) :
    QObject(parent)
{
}


ItemPrimitiv::ItemPrimitiv(ItemPrimitiv::ItemType type, QHash<QString, QVariant> property, QGraphicsScene *scene, QObject *parent) :
       QObject(parent) {
    Type = type;

    QLabel *gif_anim = new QLabel();
    QMovie *movie = new QMovie("t.gif");
    QGraphicsProxyWidget *proxy;

    switch (Type) {
    case ItemPrimitiv::Rectangle:
        GItem.append(new QGraphicsRectItem(0, 0, 50, 50));
        UpdateProp();
        break;
    case ItemPrimitiv::Circle:
        GItem.append(new QGraphicsEllipseItem(0, 0, 50, 50));
        UpdateProp();
        break;
    case ItemPrimitiv::GifImage:
        if (!property.contains("Src")) return;
        gif_anim = new QLabel();
        movie = new QMovie(property["Src"].toString());
        gif_anim->setMovie(movie);
        movie->start();
        proxy = scene->addWidget(gif_anim);
        GItem.append(proxy);
        UpdateProp();
        break;
    default:
        return ;
        break;
    }
}

QGraphicsItem *ItemPrimitiv::GetMainGraphicsItem() {
    if (GItem.isEmpty()) return 0;
    return GItem[0];
}

QHash<QString, QVariant> *ItemPrimitiv::GetProperty() {
    return &Property;
}

void ItemPrimitiv::UpdateItem() {
}

void ItemPrimitiv::UpdateProp() {
    switch (Type) {
    case ItemPrimitiv::NoItem:
        Property[""] = 0;
        break;
    case ItemPrimitiv::Rectangle:
        Property["X"] = GItem[0]->x();
        Property["Y"] = GItem[0]->y();
        Property["Z"] = GItem[0]->zValue();
        Property["Color"] = ((QGraphicsRectItem*)GItem[0])->brush().color();
        Property["Scale"] = GItem[0]->scale();
        Property["Rotation"] = GItem[0]->rotation();
        break;
    case ItemPrimitiv::Circle:
        Property["X"] = GItem[0]->x();
        Property["Y"] = GItem[0]->y();
        Property["Z"] = GItem[0]->zValue();
        Property["Color"] = ((QGraphicsEllipseItem*)GItem[0])->brush().color();
        Property["Scale"] = GItem[0]->scale();
        Property["Rotation"] = GItem[0]->rotation();
        break;
    default:
        return ;
        break;
    }
}
